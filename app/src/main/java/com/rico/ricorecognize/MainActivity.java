package com.rico.ricorecognize;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rico.ricorecognize.utils.AndroidUtils;
import com.rico.ricorecognize.utils.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {


    private FaceRecongnizer faceRecongnizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidUtils.verifyCameraPermission(MainActivity.this,1);
        AndroidUtils.verifyStoragePermissions(MainActivity.this,2);
        AndroidUtils.verifyPhonePermission(MainActivity.this,3);

        // Example of a call to a native method
        final TextView tv = (TextView) findViewById(R.id.sample_text);
        faceRecongnizer=new FaceRecongnizer(this);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean ret=faceRecongnizer.init("0036BEE63B09");
                if(ret){
                    Log.i("rico","初始成功");
                }else{
                    Log.i("rico","初始失败");
                }
            }
        });

        Button btnSave=(Button)findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FileUtils.write(iface.stringFromMD5().getBytes(),new File(FileUtils.SDCARD_PATH+"charts.txt"));
            }
        });
    }

    public static String loadFileAsString(String filePath) throws java.io.IOException{
        StringBuffer data = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            data.append(readData);
        }
        reader.close();
        return data.toString();
    }

    public String getMacAddress(){
        try {
            return loadFileAsString("/sys/class/net/eth0/address")
                    .toUpperCase().substring(0, 17);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
