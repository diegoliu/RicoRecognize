package com.rico.ricorecognize;

import android.app.Activity;
import android.widget.Toast;

public class FaceRecongnizer {
	
	private Activity activity;
	public FaceRecongnizer(Activity activity) {
		this.activity=activity;
	}
	
	//处理原始注册照片, 加载so后调用, -1无效
	public int ProcessOrignalPhotos(String photodir)
	{
		return nativeProcessOrignalPhotos(photodir);
	}
	
	
	//处理考勤，探测抓的照片后调用, -1无效
	public int ProcessGrabPhotos(String photofilename)
	{
		//return -1;
		return nativeProcessGrabPhotos(photofilename);
	}


	public boolean init(String license){
		int ret=this.nativeLibInit(license);
		if(ret==1){
			Toast.makeText(this.activity,"初始成功！", Toast.LENGTH_LONG);
			return true;
		}else{
			Toast.makeText(this.activity,"初始化失败，请确认license是否正确", Toast.LENGTH_LONG);
			return false;
		}
	}
	
	
	//0--不匹配, 1--匹配
	public int GetMatchResult(String filename, String path, int limit)
	{
		return nativeGetMatchResult(filename, path, limit);
	}
	
	private native int nativeProcessOrignalPhotos(String photodir);
	private native int nativeProcessGrabPhotos(String photofilename);

	private native int nativeGetMatchResult(String filename, String path, int limit);

	private native int nativeLibInit(String license);

	static {
		System.loadLibrary("mingxin_lib");
	}
}

