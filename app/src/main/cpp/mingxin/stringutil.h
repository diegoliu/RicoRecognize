//
// Created by Administrator on 2018/8/3.
//

#ifndef RICORECOGNIZE_STRINGUTIL_H
#define RICORECOGNIZE_STRINGUTIL_H

#include <string>


using namespace std;

namespace rico {

    bool stringEndWith(const char *str, const char *end);

}
#endif //RICORECOGNIZE_STRINGUTIL_H
