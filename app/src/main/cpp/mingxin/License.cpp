//
// Created by Administrator on 2018/8/10.
//
#include "License.h"
#include "fileutil.h"
#include "AES.h"
using namespace rico;

string License::createLicense() {
    char miwen_hex[13];
    miwen_hex[12] = '\0';
    unsigned char key[] = "!@##$%^*&DRWERD67F6AD4696407B6094F3BB69E230E64660A69E230E64660A";
    int len = sizeof(key);
    FileUtil fileUtil;
    if (fileUtil.isExist("/sys/class/net/eth0/address")) {
        string mDeviceId = fileUtil.readFile("/sys/class/net/eth0/address");
        int index = 0;
        for (int i = 0; i < 17; i++) {
            char c = mDeviceId.c_str()[i];
            if (c == ':') {
                continue;
            }
            miwen_hex[index] = key[c % len];
            index++;
        }
        return miwen_hex;
    }else {
        return "";
    }
}