//
// Created by Administrator on 2018/8/3.
//

#include "stringutil.h"
using namespace rico;


bool rico::stringEndWith(const char *str, const char *end){
    bool result = false;
    if (str != NULL && end != NULL) {
        int l1 = strlen(str);
        int l2 = strlen(end);
        if (l1 >= l2) {
            if (strcmp(str + l1 - l2, end) == 0) {
                result = true;
            }
        }
    }

    return result;
}
