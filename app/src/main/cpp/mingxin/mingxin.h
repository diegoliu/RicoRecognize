//
// Created by Administrator on 2018/8/9.
//

#ifndef RICORECOGNIZE_MINGXIN_H
#define RICORECOGNIZE_MINGXIN_H

#include "string"


class MingXin{
    public:
        MingXin();
        bool check(std::string license);
    private:
        std::string createLicense();
        static bool checkWithDeviceIsOK;
        static bool checkWithNetIsOK;
        static bool checkThreadIsRun;
        static std::string deviceId;
        void checkWithDevice(char* license);
        void initCheckWithNet(std::string deviceId);
        static void* checkWithNet(void* args);

};
#endif
