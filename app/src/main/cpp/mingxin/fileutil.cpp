//
// Created by Administrator on 2018/8/3.
//

#include "fileutil.h"
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "stringutil.h"

using namespace std;
using namespace rico;

void FileUtil::listFiles(string filePath,vector<string>& files){
    struct stat s_buf;
    char const*path = filePath.c_str();
    /*获取文件信息，把信息放到s_buf中*/
    stat(path,&s_buf);
    if(S_ISDIR(s_buf.st_mode))
    {

        struct dirent *filename;
        DIR *dp = opendir(path);
        while(filename = readdir(dp))
        {
            if(strcmp(filename->d_name, "..") == 0){
                continue;
            }
            if(strcmp(filename->d_name, ".") == 0){
                continue;
            }
            /*判断一个文件是目录还是一个普通文件*/
            char file_path[200];
            bzero(file_path,200);
            strcat(file_path,filePath.c_str());
            if(stringEndWith(filePath.c_str(),"/")==false)
            {
                strcat(file_path, "/");
            }
            strcat(file_path,filename->d_name);
            /*获取文件信息，把信息放到s_buf中*/
            stat(file_path,&s_buf);
            /*判断是否目录*/
            if(S_ISDIR(s_buf.st_mode))
            {
                listFiles(file_path,files);
            }
            /*判断是否为普通文件*/
            if(S_ISREG(s_buf.st_mode))
            {
                files.push_back(file_path);
            }
        }
        closedir(dp);
        delete(dp);
        //delete(path);
    }
    else if(S_ISREG(s_buf.st_mode))
    {
        files.push_back(filePath);
        printf("[%s] is a regular file\n",path);
    }
};

char* FileUtil::readFile(char *file) {
    FILE *fp = NULL;
    long length=fileSize(file);
    char buff[length];
    fp = fopen(file, "r");
    fgets(buff, length, (FILE*)fp);
    fclose(fp);
    return buff;
}

long FileUtil::fileSize(char *file) {
    FILE *fp=fopen(file,"rb");//打开文件
    long size=0;
    fseek(fp,0,SEEK_END);
    size=ftell(fp);
    fclose(fp);
    return size;
}

bool FileUtil::isExist(char *file) {
    if( (access(file, F_OK )) != -1 ){
        return true;
    }else{
        return false;
    }
}

int FileUtil::removeFile(char *file) {
    return remove(file);
}
