//
// Created by Administrator on 2018/8/3.
//

#ifndef RICORECOGNIZE_FILEUTIL_H
#define RICORECOGNIZE_FILEUTIL_H




#include <string>
#include<vector>


using namespace std;

namespace rico{

    class FileUtil
    {
        public:
            void listFiles(string filePath, vector<string>& files);
            char * readFile(char *file);
            long fileSize(char *file);
            bool isExist(char *file);
            int removeFile(char *file);
    };

}

#endif //RICORECOGNIZE_FILEUTIL_H
