//
// Created by Administrator on 2018/8/4.
//

#ifndef RICORECOGNIZE_CONSTANTS_H
#define RICORECOGNIZE_CONSTANTS_H
#include <android/log.h>

#define IS_DEBUG
#ifdef IS_DEBUG
#define LOG_TAG ("IFACE")
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO   , LOG_TAG, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR  , LOG_TAG, __VA_ARGS__))
#else
#define LOGI(LOG_TAG, ...) NULL
#define LOGE(LOG_TAG, ...) NULL
#endif


#define HAARCASCADE_FRONTALFACE "haarcascade_frontalface_alt.xml"
#define HAARCASCADE_EYE "haarcascade_eye_tree_eyeglasses.xml"

#endif //RICORECOGNIZE_CONSTANTS_H
