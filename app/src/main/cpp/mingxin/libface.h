//
// Created by Administrator on 2018/8/3.
//

#ifndef RICORECOGNIZE_LIBFACE_H
#define RICORECOGNIZE_LIBFACE_H

#include <android/log.h>

#define IS_DEBUG
#ifdef IS_DEBUG
#define LOG_TAG ("fromNDK")
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO   , LOG_TAG, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR  , LOG_TAG, __VA_ARGS__))
#else
#define LOGI(LOG_TAG, ...) NULL
#define LOGE(LOG_TAG, ...) NULL
#endif



#endif